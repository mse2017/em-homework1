clear variables;
warning('off','all');
% EM1 - Hausaufgabe 1.2 - Dominik Brosch

f1 = [cosd(60) -sind(60)]*2.25;
f2 = [-sind(30) -cosd(30)]*7.5;
f3 = [-1 0]*4.5;
fr = (f1+f2+f3)*-1;

fprintf("Result:\n\tx: %.3f\n\ty: %.3f\n\tsum: %.3f\n", [fr(1), fr(2), sqrt(fr(1)^2+fr(2)^2)]);

% plot
figure;
hold on;
grid on;
arrow([0 0],f1);
arrow(f1, f1+f2);
arrow(f1+f2, f1+f2+f3);
arrow(f1+f2+f3,f1+f2+f3+fr);
hold off;
title("Hausaufgabe 1.2");
xlabel("Kraft in x-Richtung in kN");
ylabel("Kraft in y-Richtung in kN");
axis equal;

warning('on','all');