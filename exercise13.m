clear variables;
warning('off','all');
% EM1 - Hausaufgabe 1.3 - Dominik Brosch

b = 9+1;
alphas = 1:360;
r = [0 0];
figure;
hold on;
grid on;
for a=alphas
fi = [(1+b)*cosd(a)-cosd((1+b)*a) (1+b)*sind(a)-sind((1+b)*a)];
% fprintf("Adding [%.2f %.2f]\n", fi(1), fi(2));
r = r + fi;
arrow([0 0],fi);
end
title("Hausaufgabe 1.3");
xlabel("Kraft in x-Richtung in N");
ylabel("Kraft in y-Richtung in N");
fprintf("Result: [%.2f %.2f]\n",r(1),r(2));
warning('on','all');